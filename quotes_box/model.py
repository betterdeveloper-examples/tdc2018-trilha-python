from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class Category(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), unique=True)

    def __str__(self):
        return self.name


class Quote(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    phrase = db.Column(db.String(1000), nullable=False)
    author = db.Column(db.String(100))

    category_id = db.Column(db.Integer, db.ForeignKey('category.id'))
    category = db.relationship('Category', backref=db.backref('quotes', lazy='dynamic'))

    def __str__(self):
        return '"{0}". ({1})'.format(self.phrase, self.author)
