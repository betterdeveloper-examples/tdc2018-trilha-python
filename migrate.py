"""
Script criado para viabilizar a migração usando Flask-migrate com uma instancia limpa do Flask.

Ao usar a app do Flask que contém as demais extensões as migrações não são realizadas.

"""
from flask import Flask

from quotes_box.model import db
from flask_migrate import Migrate

app = Flask(__name__)
app.config.from_object('quotes_box.config.Production')
migrate = Migrate(app, db)
