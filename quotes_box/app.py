from flask import Flask

from quotes_box.admin_view import QuotesBoxAdminView
from quotes_box.api import QuotesBoxApi
from quotes_box.model import db


def create_app(config_name):

    app = Flask(__name__)
    app.config.from_object(config_name)

    with app.app_context():
        db.init_app(app)
        db.create_all()

    with app.app_context():
        QuotesBoxApi(app)

    QuotesBoxAdminView(app)

    return app
